var express = require('express');
var router = express.Router();

/* GET swagger spec. */
router.get('/', function(req, res) {
  res.sendFile('/spec/swagger.json', {root: './public'});
});

module.exports = router;
