# TAS API Explorer

This application is a [Swagger Specification][1] for the TAS APIs. In addition
to the specifications, it includes a [Node.js][2] application that displays the
API Documentation using [Swagger UI][3] to allow interactive browsing and
testing of the API.

## Installation

You will need Node installed to run the application. You should also have
[Bower][4] and [Grunt][5] installed globally:

```bash
$ npm install -g bower grunt-cli
```

After cloning the repository, install the npm and bower dependencies:

```bash
$ npm install && bower install
```

## Running Locally

To run the node application, run the command:

```bash
$ npm start
```
You should now be able to access this by navigating to localhost:3000

For a production deployment, you can run the application with [Forever][6]:

```bash
$ forever start bin/www
```

To run in development mode with asset watching and auto-reload, run:

```bash
$ grunt
```

## Running with Docker

TODO


## Specfiles

The Swagger API spec files are located in `public/spec/`.

### Validating the spec files

You can validate the spec files using the `validate` script:

```bash
$ npm run validate ./public/spec/<spec file>
```

For example,

```
npm run validate ./public/spec/api/swagger.yml

> tas-api@0.1.0 validate /Users/mrhanlon/workspace/tacc/tas-api
> node ./bin/validate "./public/spec/api/swagger.yml"

Specification is valid!
```

### Editing the spec

Because editing YAML is easier than JSON, it is suggested that you edit the YAML
files to make changes, fixes to the spec. You can then convert the YAML spec to
JSON using the `convert` script:

```bash
$ npm run convert ./public/spec/<spec file>
```

For example,

```bash
npm run convert ./public/spec/api/swagger.yml

> tas-api@0.1.0 convert /Users/mrhanlon/workspace/tacc/tas-api
> node ./bin/convert "./public/spec/api/swagger.yml"

Specification is valid!
Writing file:  public/spec/api/swagger.json
```

The JSON output will only be written if the input spec validates without errors.
Warnings are reported, but will not halt the process. If a previous JSON output
exists it will be overwritten.

### Versions

There are currently three "versions" of the TAS API: Token-auth, TUP, and /v1.

#### Token-auth API

The spec files for the Token-auth version of the API are located in
`public/spec/api/`.

This API is currently used in the TACC Mobile Web and iOS apps.


#### TUP API

TODO

The spec for the TUP version of the API are located in `public/spec/tup/`.

This API is currently used in the TACC User Portal. It uses HTTP Basic Auth
and a privileged, service account to authenticate.


#### V1 API

TODO

The spec for the /v1 version of the API are located in `public/spec/v1/`.

This API is currently used in the Chameleon Portal. It uses HTTP Basic Auth
and a privileged, service account to authenticate.


[1]: https://github.com/swagger-api/swagger-spec
[2]: https://nodejs.org/
[3]: https://github.com/swagger-api/swagger-ui
[4]: http://bower.io/
[5]: http://gruntjs.com/
[6]: https://github.com/foreverjs/forever
